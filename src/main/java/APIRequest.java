
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

public class APIRequest {

    // Request components
    /**
     * This bit of code will eventually be altered to have requests to the API
     * fire from my server for the purposes of key security.
     */
    /**
     * This is the base component of the API's request URL. The key and any
     * parameters that follow it are appended to the end of this string.
     */
    public static final String WEATHER_REQUEST_BASE = "http://api.openweathermap.org/data/2.5/weather?q=";

    // Properties
    private final URL request;
    private JsonObject responseBody;

    // Constructors
    /**
     *
     * @param cityValue
     * @param appid
     * @throws MalformedURLException
     */
    public APIRequest(String cityValue, String appid) throws MalformedURLException {
        // Build the request.
        request = new URL(WEATHER_REQUEST_BASE + cityValue + "&" + "appid=" + appid);
    }

    /**
     * A JsonParser converts the response returned from request stream into a
     * JsonObject that is stored in the responseBody member variable.
     *
     * @throws java.io.IOException
     */
    public void send() throws IOException {
        try (BufferedReader reader = new BufferedReader(
                new InputStreamReader(request.openStream())
        )) {
            // Parse the response into a JsonObject
            responseBody = new JsonParser().parse(reader).getAsJsonObject();
        }
    }

    /**
     *
     * @return
     */
    public JsonObject getResponseBody() {
        return responseBody;
    }
    
    /**
     * 
     * @param args The api key to use for calls to get weather info should be
     * passed in as the first and only argument.
     * @throws MalformedURLException
     * @throws IOException 
     */
    public static void main(String[] args) throws MalformedURLException, IOException {
        APIRequest test = new APIRequest("Chicago", args[0]);
        System.out.println(test.request);
        test.send();
        System.out.println(test.responseBody);
    }
}
